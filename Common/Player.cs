﻿using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel;

namespace TicTacToe
{
    public class Player
    {

        [BsonId]
        public int Id { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public string Email { get; set; }


        public Player() {
        }
        public Player(int id, string name, string state, string email)
        {
            Id = id;
            Name = name;
            State = state;
            Email = email;
        }
    }
}
