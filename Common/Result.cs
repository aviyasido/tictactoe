﻿namespace TicTacToe
{
    public enum Result
    {
        WINNING,
        LOSING,
        STALEMATE
    }
}
