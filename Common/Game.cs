﻿namespace TicTacToe
{
    public class Game
    {
        public int PlayerOneId { get; set; }
        public int PlayerTwoId { get; set; }
        public DateTime GameDate { get; set; }
        public string GameDuration { get; set; }
        public Result GameResult { get; set; }
        public string Winner { get; set; }

        public Game() { }

        public Game(int playerOneId, int playerTwoId, DateTime gameDate, string gameDuration, Result gameResult, string winner)
        {
            PlayerOneId = playerOneId;
            PlayerTwoId = playerTwoId;
            GameDate = gameDate;
            GameDuration = gameDuration;
            GameResult = gameResult;
            Winner = winner;
        }
    }
}
