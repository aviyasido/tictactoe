﻿using System.Net.Http;
using System.Windows;

namespace TicTacToeWPF
{
    public partial class LogInWindow : Window
    {
        private HttpClient client;
        private MainWindow mainWindow = new ();
        public LogInWindow(HttpClient client)
        {
            this.client = client;
            InitializeComponent();
        }

        private async void Start_Game_Click(object sender, RoutedEventArgs e)
        {
            string? nameX, nameO;

            if (m_IdTextBoxX.Text != m_IdTextBoxO.Text)
            {
                nameX = m_IdTextBoxX.Text;
                nameO = m_IdTextBoxO.Text;

                nameX = await mainWindow.CheckIfPlayerExist(nameX);
                nameO = await mainWindow.CheckIfPlayerExist(nameO);

                if (nameX == null)
                {
                    MessageBox.Show($"player X does't exist in system, please log in and try again");
                    LogIn_Click(sender, e);
                }
                if (nameO == null)
                {
                    MessageBox.Show($"player O does't exist in system, please log in and try again");
                    LogIn_Click(sender, e);
                }
                else
                {
                    MessageBox.Show($"ready player X {nameX} and player O {nameO}");
                    TryGameWindow game = new TryGameWindow(m_IdTextBoxX.Text, m_IdTextBoxO.Text);
                    game.Show();
                    this.Close();
                }
            }

            else MessageBox.Show("can't enter same id twice");
        }

        private void LogIn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }
    }
}
