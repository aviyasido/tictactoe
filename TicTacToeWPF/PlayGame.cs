﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace TicTacToeWPF
{
    public class PlayGame
    {
        private bool isX = true;
        public static int BOARD_LENGTH = 9;
        private State[] gameBoard = new State[BOARD_LENGTH];
        private DateTime time;
        public bool flag = false;
        private Uri X = new Uri("C:\\GitLab Projects\\tictactoe\\image\\X.jpg");
        private Uri O = new Uri("C:\\GitLab Projects\\tictactoe\\image\\O.jpg");

        public PlayGame() {
            InitializeBoard();
        }

        public void InitializeBoard()
        {
            for (int i = 0; i < BOARD_LENGTH; i++)
            {
                gameBoard[i] = State.EMPTY;
            }
            flag = false;
        }

        public State CheckForVictory()
        {
            int[,] arr = new[,]
            {
                {0, 1, 2},
                {0, 3, 6},
                {0, 4, 8},
                {2, 4, 6},
                {2, 5, 8},
                {6, 7, 8},
                {1, 4, 7},
                {3, 4 ,5}
            };

            for (int i = 0; i < BOARD_LENGTH - 1; ++i)
            {
                if ((!gameBoard[arr[i, 0]].Equals(State.EMPTY)) && 
                    gameBoard[arr[i, 0]].Equals(gameBoard[arr[i, 1]]) &&
                    gameBoard[arr[i, 1]].Equals(gameBoard[arr[i, 2]]))
                    return gameBoard[arr[i, 0]];
            }

            return CheckForDeadEnd();
        }

        private State CheckForDeadEnd()
        {
            foreach (State s in gameBoard)
            {
                if (s.Equals(State.EMPTY)) return State.EMPTY;
            }

            return State.DEAD_END;
        }

        public void SetLocation(Button button, Grid gridName)
        {
            int location = (Grid.GetRow(button) * 3) + Grid.GetColumn(button);  
            
            if (gameBoard[location].Equals(State.EMPTY))
            {
                if (isX)
                {
                    gameBoard[location] = State.X;
                    ChangeImage(Grid.GetRow(button), Grid.GetColumn(button), new BitmapImage(X), gridName);
                }

                else if (!isX)
                {
                    gameBoard[location] = State.O;
                    ChangeImage(Grid.GetRow(button), Grid.GetColumn(button), new BitmapImage(O), gridName);
                }

                isX = !isX;

                ReturnResult(CheckForVictory());
            }
        }

        private void ReturnResult(State state) 
        {
            switch (state)
            {
                case State.EMPTY:
                    return;

                case State.DEAD_END:
                    MessageBox.Show("No one won this time, it's dead heat");
                    break;

                case State.O:
                    MessageBox.Show("the winner is player O, go celebrate your victory!");
                    flag = true;
                    break;

                case State.X:
                    MessageBox.Show("the winner is player X, go celebrate your victory!");
                    flag = true;
                    break;
            }     
        }

        private void ChangeImage(int row, int col, ImageSource newImage, Grid grid)
        {
            Button button = (Button)grid.Children
                .Cast<UIElement>()
                .First(e => Grid.GetRow(e) == row && Grid.GetColumn(e) == col);
            ImageBrush brush = new ImageBrush();
            brush.ImageSource = newImage;
            brush.Stretch = Stretch.Uniform;
            button.Background = brush;
        }
    }
}
