﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Xps.Packaging;
using System.Xml.Linq;
using TicTacToe;

namespace TicTacToeWPF
{ 
    public partial class MainWindow : Window
    {
        bool isX = true;
        static HttpClient client = new HttpClient();
        private PlayGame playGame = new PlayGame();
        public MainWindow()
        {           
            InitializeComponent();
        }

        private async void Details_Button_ClickX(object sender, RoutedEventArgs e)
        {
            string playerID = m_IdTextBoxX.Text;
            string playerName = m_NameTextBoxX.Text;
            string playerState = m_StateTextBoxX.Text;
            string playerEmail = m_EmailTextBoxX.Text;

            if (CheckIfNotNull(playerID, playerName, playerState, playerEmail))
            {
                if (await CheckIfPlayerExist(playerID) == null)
                {
                    Button button = (Button)sender;
                    button.IsEnabled = false;

                    AddPlayer(int.Parse(playerID), playerName, playerState, playerEmail);

                    MessageBox.Show("player 1 created successfully");
                }
                else MessageBox.Show("player alreay exist in system");
            }
            else MessageBox.Show("missing parameters, please check again");
        }

        private async void Details_Button_ClickO(object sender, RoutedEventArgs e)
        {
            string playerID = m_IdTextBoxO.Text;
            string playerName = m_NameTextBoxO.Text;
            string playerState = m_StateTextBoxO.Text;
            string playerEmail = m_EmailTextBoxO.Text;

            if (CheckIfNotNull(playerID, playerName, playerState, playerEmail))
            {
                if (await CheckIfPlayerExist(playerID) == null)
                {
                    Button button = (Button)sender;
                    button.IsEnabled = false;

                    AddPlayer(int.Parse(playerID), playerName, playerState, playerEmail);

                    MessageBox.Show("player 2 created successfully");
                }
                else MessageBox.Show("player alreay exist in system");
            }
            else MessageBox.Show("missing parameters, please check again");
        }

        private bool CheckIfNotNull(string ID, string Name, string State, string Email)
        {
            if (!string.IsNullOrWhiteSpace(ID) &&
                !string.IsNullOrWhiteSpace(Name) &&
                !string.IsNullOrWhiteSpace(State) &&
                !string.IsNullOrWhiteSpace(Email)) return true;
            return false;
        }

        private void Login_Button(object sender, RoutedEventArgs e)
        {
            LogInWindow logIn = new LogInWindow(client);
            logIn.Show();
            this.Close();
        }

        public async Task<string?> CheckIfPlayerExist(string ID)
        {
            HttpResponseMessage message = await client.GetAsync($"https://localhost:7298/api/TicTacToe/GetPlayerById/{ID}");

            if (message.StatusCode == HttpStatusCode.OK)
            {
                var responseString = await message.Content.ReadAsStringAsync();
                var bsonObject = BsonDocument.Parse(responseString);
                return bsonObject["name"].AsString;
            }

            return null;
        }
        public async Task<Player?> RetrivePerson(string ID)
        {
            HttpResponseMessage message = await client.GetAsync($"https://localhost:7298/api/TicTacToe/GetPlayerById/{ID}");
            if (message.StatusCode == HttpStatusCode.OK)
            {
                var content = await message.Content.ReadAsStringAsync();
                var bson = BsonDocument.Parse (content);
                Player player = new Player
                {
                    Id = bson["id"].AsInt32,
                    Name = bson["name"].AsString,
                    State = bson["state"].AsString,
                    Email = bson["email"].AsString,
                };
                return player;
            }
            return null;
        }
        

        private async void AddPlayer(int playerID, string playerName, string playerState, string playerEmail)
        {
            Player player = new Player(playerID,
                                       playerName,
                                       playerState,
                                       playerEmail);

            string json = JsonSerializer.Serialize(player);
            HttpContent content = new StringContent(json, new MediaTypeHeaderValue("application/json"));
            HttpResponseMessage message = await client.PostAsync($"https://localhost:7298/api/TicTacToe/AddPlayer", content);
        }
    }
}