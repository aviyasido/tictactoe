﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TicTacToe;

namespace TicTacToeWPF
{
    public partial class TryGameWindow : Window
    {
        private PlayGame playGame = new PlayGame();
        private IList<Button> buttons = new List<Button>();
        private MainWindow mainWindow = new();
        public Player? PlayerX { get; set; }
        public Player? PlayerO { get; set; }

        public event PropertyChangedEventHandler? PropertyChanged;

        public TryGameWindow(string playerXid, string playerOid)
        {
            InitializeComponent();
            DataContext = this;
            PlayerInit(playerXid, playerOid);
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!playGame.flag && sender is Button button)
            {
                buttons.Add(button);
                playGame.SetLocation(button, MainGrid);
            }
            else MessageBox.Show("can't continue the game after player won");
        }

        private void StartNewGame_Click(object sender, RoutedEventArgs e)
        {
            while (buttons.Count > 0)
            {
                buttons[0].Content = "";
                buttons.Remove(buttons[0]);
                playGame.InitializeBoard();
            }
        }

        private async void PlayerInit(string X_ID, string O_ID)
        {
            PlayerX = await mainWindow.RetrivePerson(X_ID);
            PlayerO = await mainWindow.RetrivePerson(O_ID);

            OnPropertyChanged(nameof(PlayerX));
            OnPropertyChanged(nameof(PlayerO));
        }
    }
}
