using MongoDB.Driver;
using TicTacToe;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
IServiceCollection services = builder.Services;

// Add services to the container.

string? mongoConnectionString = builder.Configuration.GetConnectionString("Mongo");

IMongoClient mongoClient = new MongoClient(mongoConnectionString);
IMongoCollection<Player> playerCollection = mongoClient
                .GetDatabase("TicTacToe")
                .GetCollection<Player>("Players");

IMongoCollection<Game> gameCollection = mongoClient
                .GetDatabase("TicTacToe")
                .GetCollection<Game>("Games");

services.AddSingleton(playerCollection);
services.AddSingleton(gameCollection);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
