﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;

namespace TicTacToe
{
    [ApiController, Route("api/[controller]")]
    public class TicTacToeController : ControllerBase
    {
        public readonly IMongoCollection<Player> _player;
        private readonly IMongoCollection<Game> _game;

        public TicTacToeController(IMongoCollection<Player> player, IMongoCollection<Game> game)
        {
            _player = player;
            _game = game;
        }

        [HttpGet, Route("GetPlayerById/{Id}")]
        public IActionResult GetPlayerById(int Id)
        {
            Player player = _player.Find(player => player.Id == Id)
                .FirstOrDefault();
            if (player == null)
            {
                return NotFound();
            }
            return Ok(player);
        }

        [HttpPost, Route("AddPlayer")]
        public IActionResult AddPlayer([FromBody] Player player)
        {
            _player.InsertOne(player);
            return Created("/TicTacToe/Players", player);
        }

        [HttpPost, Route("AddNewGame")]
        public IActionResult AddNewGameResult([FromBody] Game game)
        {
            _game.InsertOne(game);
            return Created("/TicTacToe/Games", game);
        }
    }
}

