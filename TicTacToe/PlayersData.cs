﻿namespace TicTacToe
{
    public class PlayersData
    {
        public Player playerX { get; set; }
        public Player playerO { get; set; }
    }
}
